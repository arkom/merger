package com.company;

import java.util.*;

public class Merger {

    public List<User> merge(List<User> users){
        List<User> result = new ArrayList();
        boolean needSecondStage = false;
        for (User user: users){
            boolean shouldAdd = true;
            int counter = 0;
            for (User resultUser: result){
                if (!Collections.disjoint(user.getEmails(), resultUser.getEmails())){
                    resultUser.getEmails().addAll(user.getEmails());
                    shouldAdd = false;
                    counter++;
                }
            }
            if (shouldAdd){
                result.add(user);
            }
            if (counter > 1){
                needSecondStage = true;
            }
        }
        if (needSecondStage){
            result = merge(result);
        }
        return result;
    }
}
