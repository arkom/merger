package com.company;


import java.util.HashSet;
import java.util.Set;

public class UserReader {
    public static User read(String line) {
        if (line == null || line.trim().isEmpty()) {
            return null;
        }
        String[] parts = line.split(":");
        if (parts.length != 2) {
            return null;
        }

        User user = new User(parts[0].trim());
        String[] emails = parts[1].split(",");
        Set<String> userEmails = new HashSet();
        for (String email : emails) {
            String trim = email.trim();
            if (!trim.isEmpty()) {
                userEmails.add(trim);
            }
        }

        user.getEmails().addAll(userEmails);
        return user;
    }
}
