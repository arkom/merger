package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("enter user data userName: email1, email2 (empty string for exit):");
        String line;
        List<User> users = new ArrayList();
        while (!(line = in.nextLine()).trim().isEmpty()) {
            User user = UserReader.read(line);
            if (user == null) {
                System.out.println("incorrect input = " + line);
            }
            users.add(user);
        }
        in.close();

        users.forEach(r -> System.out.println(r));

        Merger merger = new Merger();
        List<User> result = merger.merge(users);
        result.forEach(r -> System.out.println(r));

    }
}
