package com.company;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class MergerTest {

    public static final String XXX_YA_RU = "xxx@ya.ru";
    public static final String FOO_GMAIL_COM = "foo@gmail.com";
    public static final String LOL_MAIL_RU = "lol@mail.ru";
    public static final String UPS_PISEM_NET = "ups@pisem.net";
    public static final String XYZ_PISEM_NET = "xyz@pisem.net";
    public static final String VASYA_PUPKIN_COM = "vasya@pupkin.com";
    public static final String AAA_BBB_RU = "aaa@bbb.ru";

    @Test
    void merge() {
        List<User> users = getUsers();
        Merger merger = new Merger();
        List<User> result = merger.merge(users);
        result.forEach(r-> System.out.println("r = " + r));

        assertNotNull(result);
        assertEquals(2, result.size());

        assertEquals("user1", result.get(0).getName());
        assertEquals("user3", result.get(1).getName());

        assertEquals(getExpected1(), result.get(0).getEmails());
        assertEquals(getExpected2(), result.get(1).getEmails());

    }

    @Test
    void mergeSecondStage(){
        List<User> users = new ArrayList<>();
        User user1 = new User("user1");
        user1.getEmails().add(XXX_YA_RU);

        User user2 = new User("user2");
        user2.getEmails().add(FOO_GMAIL_COM);

        User user3 = new User("user1");
        user3.getEmails().add(XXX_YA_RU);
        user3.getEmails().add(FOO_GMAIL_COM);
        user3.getEmails().add(LOL_MAIL_RU);

        users.add(user1);
        users.add(user2);
        users.add(user3);

        Merger merger = new Merger();
        List<User> result = merger.merge(users);
        result.forEach(r-> System.out.println("r = " + r));
        assertNotNull(result);
        assertEquals(1, result.size());

        Set<String> expected = new HashSet<>();
        expected.add(XXX_YA_RU);
        expected.add(FOO_GMAIL_COM);
        expected.add(LOL_MAIL_RU);
        assertEquals(expected, result.get(0).getEmails());

    }

    private List<User> getUsers() {
        User user1 = new User("user1");
        user1.getEmails().add(XXX_YA_RU);
        user1.getEmails().add(FOO_GMAIL_COM);
        user1.getEmails().add(LOL_MAIL_RU);

        User user2 = new User("user2");
        user2.getEmails().add(FOO_GMAIL_COM);
        user2.getEmails().add(UPS_PISEM_NET);

        User user3 = new User("user3");
        user3.getEmails().add(XYZ_PISEM_NET);
        user3.getEmails().add(VASYA_PUPKIN_COM);

        User user4 = new User("user4");
        user4.getEmails().add(UPS_PISEM_NET);
        user4.getEmails().add(AAA_BBB_RU);

        User user5 = new User("user5");
        user5.getEmails().add(XYZ_PISEM_NET);

        List<User> users = new ArrayList();
        users.add(user1);
        users.add(user2);
        users.add(user3);
        users.add(user4);
        users.add(user5);
        return users;
    }

    private Set<String> getExpected2() {
        Set<String> expected = new HashSet();
        expected.add(XYZ_PISEM_NET);
        expected.add(VASYA_PUPKIN_COM);
        return expected;
    }

    private Set<String> getExpected1() {
        Set<String> expected1 = new HashSet();
        expected1.add(XXX_YA_RU);
        expected1.add(FOO_GMAIL_COM);
        expected1.add(LOL_MAIL_RU);
        expected1.add(UPS_PISEM_NET);
        expected1.add(AAA_BBB_RU);
        return expected1;
    }
}